"use strict";

require("dotenv").config();
const app = require("fastify")({
    logger: false,
    bodyLimit: 104857600,
});

const path = require("path");
const swagger = require("./plugins/swagger");
const multer = require("fastify-multer");
const ajv = require("ajv")({
    removeAdditional: true,
    useDefaults: true,
    coerceTypes: true,
    allErrors: true,
    nullable: true,
});

// add schema type
app.setValidatorCompiler(function (schema) {
    ajv.addFormat("file", () => {
        return true;
    });
    return ajv.compile(schema);
});
//public assets
app.register(require("fastify-static"), {
    root: path.join(__dirname, "public"),
    prefix: "/public/",
    list: false,
});
app.register(multer.contentParser);
app.register(require("fastify-swagger"), swagger.options);
app.register(require("fastify-cors"), {
    origin: (origin, cb) => {
        cb(null, true);
        return;
    },
});
app.register(require("fastify-formbody"));
app.register(require("./middleware/authentication"));
app.register(require("./app/v1/routes"));

const mainSchema = {
    schema: {
        tags: ["Main"],
        response: {
            200: {
                description: "Successful response",
                type: "object",
                properties: {
                    message: { type: "string", example: "it works!" },
                },
            },
        },
    },
};
app.get("/", mainSchema, async (req, res) => {
    return res.code(200).send({ message: "it works!" });
});

const start = async () => {
    try {
        await app.listen(process.env.PORT || 3031, "0.0.0.0");
        app.swagger();
        console.info(`Server listening at ${process.env.BASE_URL}`);
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
};
start();
