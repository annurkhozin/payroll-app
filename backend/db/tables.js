"use strict";

const table = {
    UUID: "(UUID())",
    IMAGE_ASSETS: "public/images/",

    USER_LEVEL_TABLE: "user_level",
    USER_TABLE: "user",
    USER_LOGIN_TABLE: "user_login",
    KARYAWAN_TABLE: "employee",
    KEHADIRAN_TABLE: "attendance",
    LEMBUR_TABLE: "overtime",
    HARI_LIBUR_KANTOR_TABLE: "public_holiday",
    GAJI_TABLE: "salary",
    MODULE_TABLE: "module",
    RULE_TABLE: "rule",
};

module.exports = table;
