"use strict";

const { USER_LEVEL_TABLE, UUID } = require("../tables");

exports.up = function (knex) {
    return knex.schema.createTable(USER_LEVEL_TABLE, (type) => {
        type.uuid("_id").primary().notNullable().defaultTo(knex.raw(UUID));
        type.string("alias", 30);
        type.boolean("activated").defaultTo(true);
        type.timestamp("created_at").notNullable().defaultTo(knex.fn.now());
        type.timestamp("updated_at");
        type.uuid("created_by");
        type.uuid("updated_by");
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable(USER_LEVEL_TABLE);
};
