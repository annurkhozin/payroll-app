"use strict";

const { KARYAWAN_TABLE, USER_TABLE, UUID } = require("../tables");

exports.up = function (knex) {
    return knex.schema.createTable(KARYAWAN_TABLE, (type) => {
        type.uuid("_id").primary().notNullable().defaultTo(knex.raw(UUID));
        type.string("employee_code", 10);
        type.string("fullname", 50);
        type.string("place_birth", 255);
        type.date("date_birth");
        type.string("gender", 15);
        type.string("position", 50);
        type.boolean("activated").defaultTo(true);
        type.double("gaji_pokok");
        type.double("tunjangan");
        type.json("fixed_component");
        type.timestamp("created_at").notNullable().defaultTo(knex.fn.now());
        type.timestamp("updated_at");
        type.uuid("created_by")
            .notNullable()
            .references("_id")
            .inTable(USER_TABLE);
        type.uuid("updated_by").references("_id").inTable(USER_TABLE);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable(KARYAWAN_TABLE);
};
