"use strict";

const { USER_TABLE, UUID, USER_LOGIN_TABLE } = require("../tables");

exports.up = function (knex) {
    return knex.schema.createTable(USER_LOGIN_TABLE, (type) => {
        type.uuid("_id").primary().notNullable().defaultTo(knex.raw(UUID));
        type.uuid("user_id").references("_id").inTable(USER_TABLE);
        type.string("token", 128);
        type.string("user_agent", 255);
        type.string("ip_address", 15);
        type.timestamp("created_at").notNullable().defaultTo(knex.fn.now());
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable(USER_LOGIN_TABLE);
};
