"use strict";

const { HARI_LIBUR_KANTOR_TABLE, UUID, USER_TABLE } = require("../tables");

exports.up = function (knex) {
    return knex.schema.createTable(HARI_LIBUR_KANTOR_TABLE, (type) => {
        type.uuid("_id").primary().notNullable().defaultTo(knex.raw(UUID));
        type.date("date");
        type.timestamp("created_at").notNullable().defaultTo(knex.fn.now());
        type.timestamp("updated_at");
        type.uuid("created_by")
            .notNullable()
            .references("_id")
            .inTable(USER_TABLE);
        type.uuid("updated_by").references("_id").inTable(USER_TABLE);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable(HARI_LIBUR_KANTOR_TABLE);
};
