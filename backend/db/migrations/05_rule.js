"use strict";

const {
    UUID,
    MODULE_TABLE,
    USER_LEVEL_TABLE,
    USER_TABLE,
    RULE_TABLE,
} = require("../tables");

exports.up = function (knex) {
    return knex.schema.createTable(RULE_TABLE, (type) => {
        type.uuid("_id").primary().notNullable().defaultTo(knex.raw(UUID));
        type.string("module_id").references("_id").inTable(MODULE_TABLE);
        type.string("level_id").references("_id").inTable(USER_LEVEL_TABLE);
        type.timestamp("created_at").notNullable().defaultTo(knex.fn.now());
        type.timestamp("updated_at");
        type.uuid("created_by")
            .notNullable()
            .references("_id")
            .inTable(USER_TABLE);
        type.uuid("updated_by").references("_id").inTable(USER_TABLE);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable(RULE_TABLE);
};
