"use strict";

const { USER_TABLE, UUID, USER_LEVEL_TABLE } = require("../tables");

exports.up = function (knex) {
    return knex.schema.createTable(USER_TABLE, (type) => {
        type.uuid("_id").primary().notNullable().defaultTo(knex.raw(UUID));
        type.uuid("user_reference");
        type.uuid("level_id").references("_id").inTable(USER_LEVEL_TABLE);
        type.string("username", 50);
        type.string("password", 255);
        type.string("fullname", 50);
        type.string("photo", 255);
        type.string("email", 50);
        type.boolean("activated").defaultTo(true);
        type.timestamp("created_at").notNullable().defaultTo(knex.fn.now());
        type.timestamp("updated_at");
        type.uuid("created_by").references("_id").inTable(USER_TABLE);
        type.uuid("updated_by").references("_id").inTable(USER_TABLE);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable(USER_TABLE);
};
