"use strict";

const { UUID, MODULE_TABLE } = require("../tables");

exports.up = function (knex) {
    return knex.schema.createTable(MODULE_TABLE, (type) => {
        type.uuid("_id").primary().notNullable().defaultTo(knex.raw(UUID));
        type.string("parent_id").references("_id").inTable(MODULE_TABLE);
        type.string("module_name", 33);
        type.string("slug", 33);
        type.string("icon", 33);
        type.string("active_icon", 33);
        type.integer("sequence", 3);
        type.boolean("is_menu").defaultTo(false);
        type.boolean("activated").defaultTo(true);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable(MODULE_TABLE);
};
