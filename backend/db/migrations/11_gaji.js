"use strict";

const { KARYAWAN_TABLE, UUID, USER_TABLE, GAJI_TABLE } = require("../tables");

exports.up = function (knex) {
    return knex.schema.createTable(GAJI_TABLE, (type) => {
        type.uuid("_id").primary().notNullable().defaultTo(knex.raw(UUID));
        type.string("employee_id")
            .notNullable()
            .references("_id")
            .inTable(KARYAWAN_TABLE);
        type.integer("year", 4);
        type.integer("month", 2);
        type.integer("not_work", 2);
        type.integer("overtime", 3);
        type.json("composition");
        type.double("gaji_pokok");
        type.double("tunjangan");
        type.double("net_salary");
        type.boolean("approved");
        type.string("note");
        type.timestamp("created_at").notNullable().defaultTo(knex.fn.now());
        type.timestamp("updated_at");
        type.timestamp("approved_at");
        type.uuid("created_by")
            .notNullable()
            .references("_id")
            .inTable(USER_TABLE);
        type.uuid("updated_by").references("_id").inTable(USER_TABLE);
        type.uuid("approved_by").references("_id").inTable(USER_TABLE);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable(GAJI_TABLE);
};
