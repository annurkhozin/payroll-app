"use strict";

const { LEMBUR_TABLE, KARYAWAN_TABLE, UUID, USER_TABLE } = require("../tables");

exports.up = function (knex) {
    return knex.schema.createTable(LEMBUR_TABLE, (type) => {
        type.uuid("_id").primary().notNullable().defaultTo(knex.raw(UUID));
        type.string("employee_id")
            .notNullable()
            .references("_id")
            .inTable(KARYAWAN_TABLE);
        type.timestamp("start");
        type.string("note_start");
        type.timestamp("end");
        type.string("note_end");
        type.boolean("approved");
        type.timestamp("created_at").notNullable().defaultTo(knex.fn.now());
        type.timestamp("updated_at");
        type.uuid("created_by")
            .notNullable()
            .references("_id")
            .inTable(USER_TABLE);
        type.uuid("updated_by").references("_id").inTable(USER_TABLE);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable(LEMBUR_TABLE);
};
