const { MODULE_TABLE } = require("../tables");

exports.seed = async function (knex) {
    const seed = Array();
    async function setSubModule(parent, slug) {
        const module = await knex(MODULE_TABLE)
            .select("_id")
            .where({ slug: parent })
            .first();

        const module_id = module ? module._id : null;
        for (let i = 0; i < slug.length; i++) {
            const key = slug[i];
            const data = {
                parent_id: module_id,
                slug: `${key}-${parent}`,
                is_menu: false,
                activated: true,
            };
            seed.push(data);
        }
    }

    await setSubModule("profile", ["read", "update"]);
    await setSubModule("users", ["create", "read", "update", "delete"]);
    await setSubModule("employees", ["create", "read", "update", "delete"]);
    await setSubModule("attendance", ["create", "read", "update", "approve"]);
    await setSubModule("overtime", ["create", "read", "update", "approve"]);
    await setSubModule("holiday", ["create", "read", "update", "approve"]);
    await setSubModule("payroll", [
        "create",
        "read",
        "update",
        "delete",
        "approve",
    ]);
    await setSubModule("my-payroll", ["read"]);

    await knex.raw("SET foreign_key_checks = 0;");

    // Inserts seed entries
    await knex(MODULE_TABLE).insert(seed);

    await knex.raw("SET foreign_key_checks = 1;");
};
