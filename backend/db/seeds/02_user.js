require("dotenv").config();
var hasher = require("node-object-hash")({ alg: "sha512" });
const { USER_LEVEL_TABLE, USER_TABLE, USER_LOGIN_TABLE } = require("../tables");

exports.seed = async function (knex) {
    const seedData = [];

    const users = await knex(USER_LEVEL_TABLE);
    users.forEach((key) => {
        var alias = key.alias.toLowerCase().replace(/\s/g, "_");
        const data = {
            level_id: key._id,
            username: alias,
            password: hasher.hash({
                payload: alias,
            }),
            fullname: `User ${key.alias}`,
            photo: `${process.env.BASE_URL}/public/images/avatar.png`,
            email: `${alias}@email.com`,
        };
        seedData.push(data);
    });

    // Disable foreign_key_checks
    await knex.raw("SET foreign_key_checks = 0;");

    // Deletes ALL existing entries
    await knex(USER_TABLE).del();
    await knex(USER_LOGIN_TABLE).del();

    // Inserts seed entries
    await knex(USER_TABLE).insert(seedData);

    // Enable foreign_key_checks
    await knex.raw("SET foreign_key_checks = 1;");
};
