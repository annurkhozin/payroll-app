const { MODULE_TABLE } = require("../tables");

exports.seed = async function (knex) {
    const data = [
        {
            module_name: "Profile",
            slug: "profile",
            icon: "people",
            active_icon: "people-fill",
            is_menu: false,
            activated: true,
        },
        {
            module_name: "Home",
            slug: "home",
            icon: "house",
            active_icon: "house-fill",
            sequence: 1,
            is_menu: true,
            activated: true,
        },
        {
            module_name: "User",
            slug: "users",
            icon: "people",
            active_icon: "people-fill",
            sequence: 2,
            is_menu: true,
            activated: true,
        },
        {
            module_name: "Employee",
            slug: "employees",
            icon: "person-badge",
            active_icon: "person-badge-fill",
            sequence: 3,
            is_menu: true,
            activated: true,
        },
        {
            module_name: "Attendance",
            slug: "attendance",
            icon: "person-bounding-box",
            active_icon: "person-bounding-box",
            sequence: 4,
            is_menu: true,
            activated: true,
        },
        {
            module_name: "Overtime",
            slug: "overtime",
            icon: "alarm",
            active_icon: "alarm-fill",
            sequence: 5,
            is_menu: true,
            activated: true,
        },
        {
            module_name: "Holiday",
            slug: "holiday",
            icon: "calendar2-x",
            active_icon: "calendar2-x-fill",
            sequence: 6,
            is_menu: true,
            activated: true,
        },
        {
            module_name: "Payroll",
            slug: "payroll",
            icon: "cash",
            active_icon: "cash-stack",
            sequence: 7,
            is_menu: true,
            activated: true,
        },
        {
            module_name: "Gajiku",
            slug: "my-payroll",
            icon: "cash",
            active_icon: "cash-stack",
            sequence: 8,
            is_menu: true,
            activated: true,
        },
    ];

    await knex.raw("SET foreign_key_checks = 0;");

    // Deletes ALL existing entries
    await knex(MODULE_TABLE).del();

    // Inserts seed entries
    await knex(MODULE_TABLE).insert(data);

    await knex.raw("SET foreign_key_checks = 1;");
};
