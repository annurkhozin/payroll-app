const { KARYAWAN_TABLE, USER_TABLE } = require("../tables");

exports.seed = async function (knex) {
    async function getOperator() {
        const users = await knex(USER_TABLE).where("username", "admin").first();
        return users ? users._id : null;
    }

    var created_by = await getOperator();

    const fixed_component = [
        {
            name: "Uang makan",
            type: 1,
            value: 500000,
        },
        {
            name: "Iuran",
            type: -1,
            value: 30000,
        },
    ];
    const data = [
        {
            employee_code: "IT-01",
            fullname: "Achmad Joni",
            place_birth: "Bojonegoro",
            date_birth: "1994-07-12",
            gender: "L",
            position: "Staff IT",
            gaji_pokok: 4000000,
            tunjangan: 1000000,
            fixed_component: JSON.stringify(fixed_component),
            created_by: created_by,
        },
        {
            employee_code: "PY-01",
            fullname: "Erika Vani",
            place_birth: "Tuban",
            date_birth: "1993-07-12",
            gender: "P",
            position: "Supervisor Payroll",
            gaji_pokok: 4000000,
            tunjangan: 1000000,
            fixed_component: JSON.stringify(fixed_component),
            created_by: created_by,
        },
        {
            employee_code: "PY-02",
            fullname: "Anisa Rahma",
            place_birth: "Malang",
            date_birth: "1995-07-12",
            gender: "P",
            position: "Staff Payroll",
            gaji_pokok: 4000000,
            tunjangan: 1000000,
            fixed_component: JSON.stringify(fixed_component),
            created_by: created_by,
        },
        {
            employee_code: "PR-01",
            fullname: "Joni Susilo",
            place_birth: "Surabaya",
            date_birth: "1996-04-22",
            gender: "L",
            position: "Staff Produksi",
            gaji_pokok: 4000000,
            tunjangan: 1000000,
            fixed_component: JSON.stringify(fixed_component),
            created_by: created_by,
        },
    ];

    await knex.raw("SET foreign_key_checks = 0;");

    // Deletes ALL existing entries
    await knex(KARYAWAN_TABLE).del();

    // Inserts seed entries
    await knex(KARYAWAN_TABLE).insert(data);
    const employee = await knex(KARYAWAN_TABLE).select(
        "_id",
        "fullname",
        "employee_code"
    );
    for (let i = 0; i < employee.length; i++) {
        const key = employee[i];
        var whereUpdate = {};
        if (key.employee_code == "IT-01") {
            whereUpdate = {
                username: "admin",
            };
        } else if (key.employee_code == "PY-01") {
            whereUpdate = {
                username: "supervisor_payroll",
            };
        } else if (key.employee_code == "PY-02") {
            whereUpdate = {
                username: "staff_payroll",
            };
        } else if (key.employee_code == "PR-01") {
            whereUpdate = {
                username: "karyawan",
            };
        }
        const update = {
            user_reference: key._id,
            fullname: key.fullname,
        };
        await knex(USER_TABLE).where(whereUpdate).update(update);
    }

    await knex.raw("SET foreign_key_checks = 1;");
};
