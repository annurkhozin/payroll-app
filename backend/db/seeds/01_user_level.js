const { USER_LEVEL_TABLE } = require("../tables");

exports.seed = async function (knex) {
    const data = [
        {
            alias: "ADMIN",
        },
        {
            alias: "STAFF PAYROLL",
        },
        {
            alias: "SUPERVISOR PAYROLL",
        },
        {
            alias: "KARYAWAN",
        },
    ];

    await knex.raw("SET foreign_key_checks = 0;");

    // Deletes ALL existing entries
    await knex(USER_LEVEL_TABLE).del();

    // Inserts seed entries
    await knex(USER_LEVEL_TABLE).insert(data);

    await knex.raw("SET foreign_key_checks = 1;");
};
