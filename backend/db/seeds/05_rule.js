const {
    MODULE_TABLE,
    USER_LEVEL_TABLE,
    RULE_TABLE,
    USER_TABLE,
} = require("../tables");

exports.seed = async function (knex) {
    async function getOperator() {
        const users = await knex(USER_TABLE).where("username", "admin").first();
        return users ? users._id : null;
    }

    var created_by = await getOperator();

    const seed = Array();
    const modules = await knex(MODULE_TABLE).select("_id", "slug");
    async function setRole(alias, access) {
        const level = await knex(USER_LEVEL_TABLE)
            .select("_id")
            .where({ alias: alias })
            .first();

        const level_id = level ? level._id : null;
        for (let i = 0; i < modules.length; i++) {
            const key = modules[i];
            if (access.includes(key.slug) || alias == "ADMIN") {
                const data = {
                    level_id: level_id,
                    module_id: key._id,
                    created_by: created_by,
                };
                seed.push(data);
            }
        }
    }

    await setRole("ADMIN", []);
    await setRole("STAFF PAYROLL", [
        "home",
        "profile",
        "read-profile",
        "update-profile",
        "employees",
        "create-employees",
        "read-employees",
        "update-employees",
        "delete-employees",
        "payroll",
        "create-payroll",
        "read-payroll",
        "update-payroll",
        "delete-payroll",
        "my-payroll",
        "read-my-payroll",
        "attendance",
        "create-attendance",
        "read-attendance",
        "update-attendance",
        "approve-attendance",
        "overtime",
        "create-overtime",
        "read-overtime",
        "update-overtime",
        "approve-overtime",
        "holiday",
        "create-holiday",
        "read-holiday",
        "update-holiday",
        "delete-holiday",
    ]);
    await setRole("SUPERVISOR PAYROLL", [
        "home",
        "profile",
        "read-profile",
        "update-profile",
        "employees",
        "read-employees",
        "payroll",
        "read-payroll",
        "approve-payroll",
        "my-payroll",
        "read-my-payroll",
        "attendance",
        "create-attendance",
        "read-attendance",
        "approve-attendance",
        "overtime",
        "create-overtime",
        "read-overtime",
        "approve-overtime",
    ]);
    await setRole("KARYAWAN", [
        "home",
        "profile",
        "read-profile",
        "update-profile",
        "my-payroll",
        "read-my-payroll",
        "attendance",
        "create-attendance",
        "read-attendance",
        "approve-attendance",
        "overtime",
        "create-overtime",
        "read-overtime",
        "approve-overtime",
    ]);
    await knex.raw("SET foreign_key_checks = 0;");

    // Deletes ALL existing entries
    await knex(RULE_TABLE).del();

    // Inserts seed entries
    await knex(RULE_TABLE).insert(seed);

    await knex.raw("SET foreign_key_checks = 1;");
};
