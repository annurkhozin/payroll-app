"use strict";

const fp = require("fastify-plugin");
const knex = require("../db/sql-connection");
const {
    USER_LOGIN_TABLE,
    USER_TABLE,
    MODULE_TABLE,
    RULE_TABLE,
} = require("../db/tables");

module.exports = fp(function (app, options, next) {
    app.addHook("preHandler", async (req, res) => {
        var protected_url = req.headers.protected
            ? req.headers.protected
            : false;

        if (protected_url) {
            var token = req.headers.authorization;
            var url_slug = req.headers.url_slug ? req.headers.url_slug : null;
            if (token && url_slug) {
                var filter = {
                    "A.token": token,
                    "B.activated": 1,
                    "D.slug": url_slug,
                    "D.activated": 1,
                };
                const access = await knex
                    .select(["A.user_id"])
                    .from(`${USER_LOGIN_TABLE} AS A`)
                    .leftJoin(`${USER_TABLE} AS B`, "A.user_id", "B._id")
                    .leftJoin(`${RULE_TABLE} AS C`, "B.level_id", "C.level_id")
                    .leftJoin(`${MODULE_TABLE} AS D`, "C.module_id", "D._id")
                    .where(filter)
                    .first();

                if (access) {
                    const account_id = access.user_id ? access.user_id : null;
                    req.headers.account_id = account_id;
                } else {
                    return res.code(403).send({
                        statusCode: 403,
                        error: "Forbidden",
                        message: {
                            id: "Tidak mempunyai akses",
                            en: "Don't have access",
                        },
                    });
                }
            } else {
                return res.code(401).send({
                    statusCode: 401,
                    error: "Unauthorized",
                    message: {
                        id: "Dibutuhkan TOKEN akses",
                        en: "Access TOKEN required",
                    },
                });
            }
        }
        return;
    });
    next();
});
