"use strict";

require("dotenv").config();
const knex = require("../../../db/sql-connection");
const {
    USER_TABLE,
    KARYAWAN_TABLE,
    GAJI_TABLE,
} = require("../../../db/tables");
var fs = require("fs");
var pdf = require("html-pdf");
const Mustache = require("mustache");

async function postData(req, res) {
    // mendapatkan pengguna
    const created_by = req.headers.account_id;
    const data = {};

    // cek properti lain yang terdapat nilai
    const properties = [
        `employee_id`,
        `year`,
        `month`,
        `composition`,
        `net_salary`,
        `approved`,
        `note`,
        `not_work`,
        `overtime`,
        `gaji_pokok`,
        `tunjangan`,
    ];
    for (var key in req.body) {
        if (properties.includes(key)) {
            var value = req.body[key] ? req.body[key] : null;
            if (value) {
                if (key == `approved`) {
                    data[key] = value == true || value == "true";
                } else {
                    data[key] = value;
                }
            }
        }
    }

    const employee_id = data.employee_id ? data.employee_id : null;
    const month = data.month ? data.month : null;
    const year = data.year ? data.year : null;
    if (employee_id && month && year) {
        const where = {
            employee_id: employee_id,
            month: month,
            year: year,
        };
        const check = await knex
            .select("_id")
            .from(GAJI_TABLE)
            .where(where)
            .first();
        if (check) {
            data["updated_by"] = created_by;
            data["updated_at"] = new Date();
            await knex.from(GAJI_TABLE).where(where).update(data);
            return res.code(201).send({
                statusCode: 201,
                message: {
                    id: "Payroll berhasil diperbarui",
                    en: "Payroll updated successfully",
                },
            });
        } else {
            data["created_by"] = created_by;
            await knex.from(GAJI_TABLE).insert(data);
            return res.code(201).send({
                statusCode: 201,
                message: {
                    id: "Payroll berhasil dibuat",
                    en: "Payroll added successfully",
                },
            });
        }
    } else {
        return res.code(400).send({
            statusCode: 400,
            error: "Not found",
            message: {
                id: "Diperlukan periode penggajian",
                en: "Payroll period required",
            },
        });
    }
}

async function getData(req, res) {
    var year = req.query.year ? req.query.year : null;
    var month = req.query.month ? req.query.month : null;
    var search = req.query.search ? req.query.search : null;
    var limit = req.query.limit ? req.query.limit : 10;
    var page = req.query.page ? req.query.page : 1;
    var order_field = req.query.order_field
        ? req.query.order_field
        : "fullname";
    var order_by = req.query.order_by ? req.query.order_by : "ASC";

    let offset = limit * (page - 1);
    let countData = 0;
    let countFilterData = 0;
    var resData = [];

    var where = {};

    const select = ["A.*"];

    // cari jumlah data keseluruhan
    countData = await knex
        .from(`${KARYAWAN_TABLE} AS A`)
        .count("A._id as total")
        .where(where)
        .then((data) => {
            return data[0]["total"];
        });

    // ketika dilakukan search data
    if (search) {
        // cari jumlah data ditemukan sesuai kata kunci
        countFilterData = await knex
            .from(`${KARYAWAN_TABLE} AS A`)
            .where(where)
            .andWhere("A.fullname", "LIKE", "%" + search + "%")
            .count("A._id as total")
            .then((data) => {
                return data[0]["total"];
            });

        // cari data sesuai kata kunci
        resData = await knex
            .select(select)
            .from(`${KARYAWAN_TABLE} AS A`)
            .where(where)
            .andWhere("A.fullname", "LIKE", "%" + search + "%")
            .offset(offset)
            .limit(limit)
            .orderBy(order_field, order_by);
    } else {
        // ketika tidak dilakukan search data
        countFilterData = countData;
        resData = await knex
            .select(select)
            .from(`${KARYAWAN_TABLE} AS A`)
            .where(where)
            .offset(offset)
            .limit(limit)
            .orderBy(order_field, order_by);
    }

    const whereGaji = {
        year: year,
        month: month,
    };
    const returnData = [];
    for (let i = 0; i < resData.length; i++) {
        const key = resData[i];

        const data = {
            _id: key._id,
            employee_code: key.employee_code,
            fullname: key.fullname,
            place_birth: key.place_birth,
            date_birth: key.date_birth,
            gender: key.gender,
            position: key.position,
            gaji_pokok: key.gaji_pokok,
            tunjangan: key.tunjangan,
            fixedComponents: JSON.parse(key.fixed_component),
            status: 1,
        };
        whereGaji["employee_id"] = key._id;
        const gaji = await knex.from(GAJI_TABLE).where(whereGaji).first();
        if (gaji) {
            data["status"] = 2;
            const approved = gaji.approved ? gaji.approved : null;
            if (approved) {
                data["gaji_pokok"] = gaji.gaji_pokok;
                data["tunjangan"] = gaji.tunjangan;
                data["status"] = 3;
            }

            data["approved"] =
                approved == 1 ? true : approved == 0 ? false : null;
            data["not_work"] = gaji.not_work;
            data["overtime"] = gaji.overtime;
        }
        returnData.push(data);
    }

    return res.code(200).send({
        statusCode: 200,
        recordsTotal: countData,
        recordsFiltered: countFilterData,
        limit: limit,
        page: page,
        data: returnData,
    });
}

async function detailData(req, res) {
    // mendapatakan ID params
    var _id = req.params._id ? req.params._id : null;

    const where = {
        "A._id": _id,
    };

    const select = ["A.*", "B.fullname AS created", "C.fullname AS updated"];
    const data = await knex
        .select(select)
        .from(`${KARYAWAN_TABLE} AS A`)
        .leftJoin(`${USER_TABLE} AS B`, "A.created_by", "B._id")
        .leftJoin(`${USER_TABLE} AS C`, "A.updated_by", "C._id")
        .where(where)
        .orWhere({ "A.employee_code": _id })
        .first();

    if (data) {
        const returnData = {
            _id: data._id,
            employee_code: data.employee_code,
            fullname: data.fullname,
            place_birth: data.place_birth,
            date_birth: data.date_birth,
            gender: data.gender,
            position: data.position,
            activated: data.activated,
            gaji_pokok: data.gaji_pokok,
            tunjangan: data.tunjangan,
            fixedComponents: JSON.parse(data.fixed_component),
            created_at: data.created_at,
            updated_at: data.updated_at,
            created_by: {
                account_id: data.created_by,
                fullname: data.created,
            },
            updated_by: {
                account_id: data.updated_by,
                fullname: data.updated,
            },
        };
        return res.code(200).send({
            statusCode: 200,
            data: returnData,
        });
    } else {
        return res.code(404).send({
            statusCode: 404,
            error: "Not found",
            message: {
                id: "ID Karyawan tidak ditemukan",
                en: "Employee ID not found",
            },
        });
    }
}

async function putData(req, res) {
    // mendapatkan pengguna
    const created_by = req.headers.account_id;
    const data = {};

    // cek properti lain yang terdapat nilai
    const properties = [
        `employee_id`,
        `year`,
        `month`,
        `composition`,
        `net_salary`,
        `approved`,
        `note`,
        `not_work`,
        `overtime`,
        `gaji_pokok`,
        `tunjangan`,
    ];
    for (var key in req.body) {
        if (properties.includes(key)) {
            var value = req.body[key] ? req.body[key] : null;
            if (value) {
                if (key == `approved`) {
                    data[key] = value == true || value == "true";
                } else {
                    data[key] = value;
                }
            }
        }
    }

    const employee_id = data.employee_id ? data.employee_id : null;
    const month = data.month ? data.month : null;
    const year = data.year ? data.year : null;
    if (employee_id && month && year) {
        const where = {
            employee_id: employee_id,
            month: month,
            year: year,
        };
        if (data["approved"] == true) {
            data["approved_by"] = created_by;
            data["approved_at"] = new Date();
        } else {
            data["updated_by"] = created_by;
            data["updated_at"] = new Date();
        }
        await knex.from(GAJI_TABLE).where(where).update(data);
        return res.code(201).send({
            statusCode: 201,
            message: {
                id: "Payroll berhasil diperbarui",
                en: "Payroll updated successfully",
            },
        });
    } else {
        return res.code(400).send({
            statusCode: 400,
            error: "Not found",
            message: {
                id: "Diperlukan periode penggajian",
                en: "Payroll period required",
            },
        });
    }
}

async function deleteData(req, res) {
    var _id = req.params._id ? req.params._id : null;

    try {
        await knex
            .from(KARYAWAN_TABLE)
            .where({ _id: _id })
            .orWhere({ employee_code: _id })
            .delete();
        return res.code(200).send({
            statusCode: 200,
            message: {
                id: "Data karyawan berhasil dihapus",
                en: "Employee data deleted successfully",
            },
        });
    } catch (error) {
        return res.code(403).send({
            statusCode: 403,
            message: {
                id: "Data karyawan tidak dapat dihapus, karena digunakan sebagai referensi data lain.",
                en: "The employee data cannot be deleted, because it is used as a reference for other data.",
            },
        });
    }
}

async function downloadSlip(req, res) {
    const html = `<div class="slip-preview">
        <center style="width: 100%; table-layout: fixed;font-family: Arial, Helvetica, sans-serif;">
          <div style="max-width: 400px;border-style: double;padding:10px;">
            <table style="border-collapse: collapse; width: 100%">
              <tbody>
                <tr>
                  <td style="width: 50%">
                    <strong>PT. Mekar Jaya</strong>
                  </td>
                  <td style="width: 50%; text-align: right">
                    <strong>SLIP GAJI</strong>
                  </td>
                </tr>
              </tbody>
            </table>
            <p style="text-align: right; margin: 0;">
              {{ month_text }} {{ year }}
            </p>
            <hr style="background-color: gray;margin: 0;" />
            <table style="border-collapse: collapse; width: 100%;">
              <tbody>
                <tr>
                  <td style="width: 30.7563%">Kode Karyawan</td>
                  <td style="width: 2.53408%">:</td>
                  <td style="width: 66.7095%">
                    {{ employee_code }}
                  </td>
                </tr>
                <tr>
                  <td style="width: 30.7563%">Nama Lengkap</td>
                  <td style="width: 2.53408%">:</td>
                  <td style="width: 66.7095%">{{ fullname }}</td>
                </tr>
                <tr>
                  <td style="width: 30.7563%">Jabatan</td>
                  <td style="width: 2.53408%">:</td>
                  <td style="width: 66.7095%">{{ position }}</td>
                </tr>
              </tbody>
            </table>
            <hr style="background-color: gray;margin: 0;" />
            <table
              style="border-collapse: collapse; width: 100%; margin-top: 15px;"
            >
              <thead>
                <tr>
                  <td style="width: 43.7485%">
                    <strong>A. Upah tetap</strong>
                  </td>
                  <td style="width: 4.4834%; text-align: center;">&nbsp;</td>
                  <td style="width: 51.7679%">&nbsp;</td>
                </tr>
              </thead>
              <tbody>
                {{ #composition.upah_tetap }}
                <tr>
                  <td style="width: 43.7485%">
                    &nbsp;&nbsp;&nbsp;- {{ name }}
                  </td>
                  <td style="width: 4.4834%; text-align: center;">:</td>
                  <td style="width: 51.7679%; text-align: right;">
                    {{ value_text }}
                  </td>
                </tr>
                 {{ /composition.upah_tetap}}
              </tbody>
            </table>
            <table
              style="
                        border-collapse: collapse;
                        width: 100%;
                        height: 18px;
                        margin-top: 10px;
                      "
            >
              <thead>
                <tr>
                  <td style="width: 43.7485%">
                    <strong>B. Upah tidak tetap</strong>
                  </td>
                  <td style="width: 4.4834%; text-align: center;">&nbsp;</td>
                  <td style="width: 51.7679%">&nbsp;</td>
                </tr>
              </thead>
              <tbody>
                 {{ #composition.upah_tidak_tetap }}
                <tr
                  
                >
                  <td style="width: 43.7485%">
                    &nbsp;&nbsp;&nbsp;- {{ name }}
                  </td>
                  <td style="width: 4.4834%; text-align: center;">:</td>
                  <td style="width: 51.7679%; text-align: right;">
                    {{ value_text }}
                  </td>
                </tr>
                {{ /composition.upah_tidak_tetap }}
              </tbody>
            </table>
            <table
              style="
                        border-collapse: collapse;
                        width: 100%;
                        margin-top: 10px;
                      "
            >
              <thead>
                <tr>
                  <td style="width: 43.7485%">
                    <strong>C. Potongan</strong>
                  </td>
                  <td style="width: 4.4834%; text-align: center;">&nbsp;</td>
                  <td style="width: 51.7679%">&nbsp;</td>
                </tr>
              </thead>
              <tbody>
                {{ #composition.potongan }}
                <tr>
                  <td style="width: 43.7485%">
                    &nbsp;&nbsp;&nbsp;- {{ name }}
                  </td>
                  <td style="width: 4.4834%; text-align: center;">:</td>
                  <td style="width: 51.7679%; text-align: right;">
                    {{ value_text }}
                  </td>
                </tr>
                {{ /composition.potongan }}
              </tbody>
            </table>
            <table style="border-collapse: collapse; width: 100%; margin-top:15px;" border="1">
              <tbody>
                <tr>
                  <td style="width: 43.7485%">
                    &nbsp;
                    <strong>Total Penerimaan</strong>
                  </td>
                  <td style="width: 4.4834%; text-align: center;">:</td>
                  <td style="width: 51.7679%">
                    &nbsp;<strong>{{ net_salary_text }}</strong>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </center>
      </div>`;

    // var view = {
    //     monthName: "Januari",
    //     year: 2021,
    //     employee_code: "IT-001",
    //     fullname: "Nur Khozin",
    //     position: "Staff IT",
    //     net_salary: "RP 5.7000.000",
    //     upah_tetap: [
    //         {
    //             name: "Gaji pokok",
    //             value: "Rp 4.000.000",
    //         },
    //         {
    //             name: "Tunjangan",
    //             value: "Rp 1.000.000",
    //         },
    //     ],
    //     upah_tidak_tetap: [
    //         {
    //             name: "Lembur",
    //             value: "Rp 500.000",
    //         },
    //     ],
    //     potongan: [
    //         {
    //             name: "BPJS",
    //             value: "Rp 150.000",
    //         },
    //         {
    //             name: "NWNP",
    //             value: "Rp 100.000",
    //         },
    //     ],
    // };

    var htmlOutput = Mustache.render(html, req.body);
    // return res.send(htmlOutput);
    // var html = ``;
    var options = {
        format: "Letter",
        border: {
            top: "1cm", // default is 0, units: mm, cm, in, px
            //     right: "3cm",
            //     bottom: "3cm",
            //     left: "3cm",
        },
    };
    return pdf.create(htmlOutput, options).toBuffer(function (err, buffer) {
        return res
            .header("Content-disposition", `attachment; filename=test.pdf`)
            .type("application/pdf")
            .send(buffer);
    });
}

async function getSlipGaji(req, res) {
    // mendapatakan ID query
    var year = req.query.year ? req.query.year : null;
    var month = req.query.month ? req.query.month : null;
    const employee_id = req.headers.account_id;
    const user_data = await knex
        .select("user_reference")
        .from(USER_TABLE)
        .where({ _id: employee_id })
        .first();
    const user_reference = user_data.user_reference;
    const where = {
        year: parseInt(year),
        month: parseInt(month),
        employee_id: user_reference,
    };

    const data = await knex.from(GAJI_TABLE).where(where).first();

    if (data) {
        data["composition"] = JSON.parse(data["composition"]);

        return res.code(200).send({
            statusCode: 200,
            data: data,
        });
    } else {
        return res.code(404).send({
            statusCode: 404,
            error: "Not found",
            message: {
                id: "Slip Gaji pada periode yang dipilih belum dibuat",
                en: "Salary Slip for the selected period has not been created",
            },
        });
    }
}

module.exports = {
    postData,
    getData,
    detailData,
    putData,
    deleteData,
    downloadSlip,
    getSlipGaji,
};
