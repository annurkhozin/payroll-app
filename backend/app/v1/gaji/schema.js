"use strict";

const tags = ["Penggajian"];

const postData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        body: {
            type: "object",
            required: [
                "employee_id",
                "year",
                "month",
                "composition",
                "net_salary",
            ],
            properties: {
                employee_id: {
                    type: "string",
                    description: "ID Karyawan",
                },
                year: {
                    type: "number",
                    format: "year",
                    description: "Periode tahun",
                },
                month: {
                    type: "number",
                    description: "Periode Bulan",
                },
                not_work: {
                    type: "number",
                    description: "Jumlah hari tidak masuk",
                },
                overtime: {
                    type: "number",
                    description: "Jumlah jam lembur",
                },
                composition: {
                    type: "string",
                    description: "JSON stringify komposisi gaji",
                },
                net_salary: {
                    type: "double",
                    description: "Gaji diterima",
                },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "create-payroll";
        done();
    },
};

const getData = {
    schema: {
        tags,
        query: {
            type: "object",
            properties: {
                activated: { type: "boolean" },
                search: { type: "string" },
                limit: { type: "integer", description: "Default [10]" },
                page: { type: "integer" },
                order_field: {
                    type: "string",
                    description: "Default [fullname]",
                    enum: ["fullname ", "created_at", "updated_at"],
                },
                order_by: {
                    type: "string",
                    description: "Default [DESC]",
                    enum: ["ASC", "DESC"],
                },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = false;
        done();
    },
};

const detailData = {
    schema: {
        tags,
        params: {
            type: "object",
            required: ["_id"],
            properties: {
                _id: { type: "string", description: "ID / kode karyawan" },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = false;
        done();
    },
};

const putData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        params: {
            type: "object",
            required: ["_id"],
            properties: {
                _id: { type: "string", description: "ID / kode karyawan" },
            },
        },
        body: {
            type: "object",
            properties: {
                employee_code: {
                    type: "string",
                    description: "Kode karyawan",
                },
                fullname: {
                    type: "string",
                    description: "Nama lengkap",
                },
                place_birth: {
                    type: "string",
                    description: "Tempat lahir",
                },
                date_birth: {
                    type: "string",
                    description: "Tanggal lahir",
                },
                gender: {
                    type: "string",
                    description: "Jenis kelamin",
                },
                position: {
                    type: "string",
                    description: "Jabatan",
                },
                activated: { type: "boolean" },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "approve-payroll";
        done();
    },
};

const deleteData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        params: {
            type: "object",
            required: ["_id"],
            properties: {
                _id: { type: "string", description: "ID / kode karyawan" },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "delete-payroll";
        done();
    },
};

const downloadSlip = {
    schema: {
        tags,
        body: {
            type: "object",
            properties: {
                params: { type: "string" },
            },
        },
    },
    onRequest: function (req, res, done) {
        done();
    },
};
const getSlipGaji = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        query: {
            type: "object",
            properties: {
                year: { type: "integer", description: "Periode tahun" },
                month: { type: "integer", description: "Periode bulan" },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "read-my-payroll";
        done();
    },
};

module.exports = {
    postData,
    getData,
    detailData,
    putData,
    deleteData,
    downloadSlip,
    getSlipGaji,
};
