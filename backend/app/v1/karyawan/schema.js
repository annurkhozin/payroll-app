"use strict";

const tags = ["Karyawan"];

const postData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        body: {
            type: "object",
            required: [
                "employee_code",
                "fullname",
                "place_birth",
                "date_birth",
                "gender",
                "position",
            ],
            properties: {
                employee_code: {
                    type: "string",
                    description: "Kode karyawan",
                },
                fullname: {
                    type: "string",
                    description: "Nama lengkap",
                },
                place_birth: {
                    type: "string",
                    description: "Tempat lahir",
                },
                date_birth: {
                    type: "string",
                    description: "Tanggal lahir",
                },
                gender: {
                    type: "string",
                    description: "Jenis kelamin",
                },
                position: {
                    type: "string",
                    description: "Jabatan",
                },
                activated: { type: "boolean", description: "Default [true]" },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "create-employees";
        done();
    },
};

const getData = {
    schema: {
        tags,
        query: {
            type: "object",
            properties: {
                activated: { type: "boolean" },
                search: { type: "string" },
                limit: { type: "integer", description: "Default [10]" },
                page: { type: "integer" },
                order_field: {
                    type: "string",
                    description: "Default [fullname]",
                    enum: ["fullname ", "created_at", "updated_at"],
                },
                order_by: {
                    type: "string",
                    description: "Default [DESC]",
                    enum: ["ASC", "DESC"],
                },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = false;
        done();
    },
};

const detailData = {
    schema: {
        tags,
        params: {
            type: "object",
            required: ["_id"],
            properties: {
                _id: { type: "string", description: "ID / kode karyawan" },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = false;
        done();
    },
};

const putData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        params: {
            type: "object",
            required: ["_id"],
            properties: {
                _id: { type: "string", description: "ID / kode karyawan" },
            },
        },
        body: {
            type: "object",
            properties: {
                employee_code: {
                    type: "string",
                    description: "Kode karyawan",
                },
                fullname: {
                    type: "string",
                    description: "Nama lengkap",
                },
                place_birth: {
                    type: "string",
                    description: "Tempat lahir",
                },
                date_birth: {
                    type: "string",
                    description: "Tanggal lahir",
                },
                gender: {
                    type: "string",
                    description: "Jenis kelamin",
                },
                position: {
                    type: "string",
                    description: "Jabatan",
                },
                activated: { type: "boolean" },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "update-employees";
        done();
    },
};

const deleteData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        params: {
            type: "object",
            required: ["_id"],
            properties: {
                _id: { type: "string", description: "ID / kode karyawan" },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "delete-employees";
        done();
    },
};

module.exports = {
    postData,
    getData,
    detailData,
    putData,
    deleteData,
};
