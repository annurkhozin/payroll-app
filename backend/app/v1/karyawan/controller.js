"use strict";

require("dotenv").config();
const knex = require("../../../db/sql-connection");
const { USER_TABLE, KARYAWAN_TABLE } = require("../../../db/tables");

async function postData(req, res) {
    // mendapatkan pengguna
    const created_by = req.headers.account_id;
    const data = {
        created_by: created_by,
    };

    // cek properti lain yang terdapat nilai
    const properties = [
        `employee_code`,
        `fullname`,
        `place_birth`,
        `date_birth`,
        `gender`,
        `position`,
        `gaji_pokok`,
        `tunjangan`,
        `fixedComponents`,
        `activated`,
    ];
    for (var key in req.body) {
        if (properties.includes(key)) {
            var value = req.body[key] ? req.body[key] : null;
            if (value) {
                if (key == `activated`) {
                    data[key] = value == true || value == "true";
                } else if (key == "date_birth") {
                    data[key] = new Date(value);
                } else if (key == "fixedComponents") {
                    console.log(value);
                } else {
                    data[key] = value;
                }
            }
        }
    }

    var fullname = data.fullname ? data.fullname : null;
    var place_birth = data.place_birth ? data.place_birth : null;
    var date_birth = data.date_birth ? data.date_birth : null;
    var gender = data.gender ? data.gender : null;
    var position = data.position ? data.position : null;
    var employee_code = data.employee_code
        ? data.employee_code.toUpperCase()
        : null;
    if (employee_code) {
        const available = await knex
            .from(KARYAWAN_TABLE)
            .select("_id")
            .where("employee_code", employee_code)
            .first();
        if (available) {
            return res.code(400).send({
                statusCode: 400,
                error: "Bad request",
                message: {
                    id: "Kode karyawan sudah digunakan",
                    en: "Employee code already used",
                },
            });
        }
    }

    // cek properti yang wajib diisi
    if (!employee_code) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Kode karyawan wajib diisi",
                en: "Employee code is required",
            },
        });
    } else if (!fullname) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Nama pengguna wajib diisi",
                en: "Fullname is required",
            },
        });
    } else if (!place_birth) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Tempat lahir wajib diisi",
                en: "Place of birth is required",
            },
        });
    } else if (!date_birth) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Tanggal lahir wajib diisi",
                en: "Date of birth is required",
            },
        });
    } else if (!gender) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Jenis kelamin wajib diisi",
                en: "Gender is required",
            },
        });
    } else if (!position) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Jabatan wajib diisi",
                en: "Position is required",
            },
        });
    } else {
        // menyimpan data berdasarkan properti yang terisi
        await knex.from(KARYAWAN_TABLE).insert(data);

        return res.code(201).send({
            statusCode: 201,
            message: {
                id: "Karyawan berhasil ditambahkan",
                en: "Employee added successfully",
            },
        });
    }
}

async function getData(req, res) {
    var activated = req.query.activated ? req.query.activated : null;
    var search = req.query.search ? req.query.search : null;
    var limit = req.query.limit ? req.query.limit : 10;
    var page = req.query.page ? req.query.page : 1;
    var order_field = req.query.order_field
        ? req.query.order_field
        : "fullname";
    var order_by = req.query.order_by ? req.query.order_by : "ASC";

    let offset = limit * (page - 1);
    let countData = 0;
    let countFilterData = 0;
    var resData = [];

    var where = {};

    if (activated !== null) {
        where = {
            "A.activated": activated == `true`,
        };
    }
    const select = ["A.*", "B.fullname AS created", "C.fullname AS updated"];

    // cari jumlah data keseluruhan
    countData = await knex
        .from(`${KARYAWAN_TABLE} AS A`)
        .count("A._id as total")
        .where(where)
        .then((data) => {
            return data[0]["total"];
        });

    // ketika dilakukan search data
    if (search) {
        // cari jumlah data ditemukan sesuai kata kunci
        countFilterData = await knex
            .from(`${KARYAWAN_TABLE} AS A`)
            .where(where)
            .andWhere("A.fullname", "LIKE", "%" + search + "%")
            .count("A._id as total")
            .then((data) => {
                return data[0]["total"];
            });

        // cari data sesuai kata kunci
        resData = await knex
            .select(select)
            .from(`${KARYAWAN_TABLE} AS A`)
            .leftJoin(`${USER_TABLE} AS B`, "A.created_by", "B._id")
            .leftJoin(`${USER_TABLE} AS C`, "A.updated_by", "C._id")
            .where(where)
            .andWhere("A.fullname", "LIKE", "%" + search + "%")
            .offset(offset)
            .limit(limit)
            .orderBy(order_field, order_by);
    } else {
        // ketika tidak dilakukan search data
        countFilterData = countData;
        resData = await knex
            .select(select)
            .from(`${KARYAWAN_TABLE} AS A`)
            .leftJoin(`${USER_TABLE} AS B`, "A.created_by", "B._id")
            .leftJoin(`${USER_TABLE} AS C`, "A.updated_by", "C._id")
            .where(where)
            .offset(offset)
            .limit(limit)
            .orderBy(order_field, order_by);
    }

    const returnData = [];
    resData.forEach((key) => {
        const data = {
            _id: key._id,
            employee_code: key.employee_code,
            fullname: key.fullname,
            place_birth: key.place_birth,
            date_birth: key.date_birth,
            gender: key.gender,
            position: key.position,
            activated: key.activated,
            gaji_pokok: key.gaji_pokok,
            tunjangan: key.tunjangan,
            fixedComponents: JSON.parse(key.fixed_component),
            created_at: key.created_at,
            updated_at: key.updated_at,
            created_by: {
                account_id: key.created_by,
                fullname: key.created,
            },
            updated_by: {
                account_id: key.updated_by,
                fullname: key.updated,
            },
        };
        returnData.push(data);
    });

    return res.code(200).send({
        statusCode: 200,
        recordsTotal: countData,
        recordsFiltered: countFilterData,
        limit: limit,
        page: page,
        data: returnData,
    });
}

async function detailData(req, res) {
    // mendapatakan ID params
    var _id = req.params._id ? req.params._id : null;

    const where = {
        "A._id": _id,
    };

    const select = ["A.*", "B.fullname AS created", "C.fullname AS updated"];
    const data = await knex
        .select(select)
        .from(`${KARYAWAN_TABLE} AS A`)
        .leftJoin(`${USER_TABLE} AS B`, "A.created_by", "B._id")
        .leftJoin(`${USER_TABLE} AS C`, "A.updated_by", "C._id")
        .where(where)
        .orWhere({ "A.employee_code": _id })
        .first();

    if (data) {
        const returnData = {
            _id: data._id,
            employee_code: data.employee_code,
            fullname: data.fullname,
            place_birth: data.place_birth,
            date_birth: data.date_birth,
            gender: data.gender,
            position: data.position,
            activated: data.activated,
            gaji_pokok: data.gaji_pokok,
            tunjangan: data.tunjangan,
            fixedComponents: JSON.parse(data.fixed_component),
            created_at: data.created_at,
            updated_at: data.updated_at,
            created_by: {
                account_id: data.created_by,
                fullname: data.created,
            },
            updated_by: {
                account_id: data.updated_by,
                fullname: data.updated,
            },
        };
        return res.code(200).send({
            statusCode: 200,
            data: returnData,
        });
    } else {
        return res.code(404).send({
            statusCode: 404,
            error: "Not found",
            message: {
                id: "ID Karyawan tidak ditemukan",
                en: "Employee ID not found",
            },
        });
    }
}

async function putData(req, res) {
    // mendapatkan ID berita
    var _id = req.params._id ? req.params._id : null;

    // mendapatkan pengguna
    const updated_by = req.headers.account_id;
    const data = {
        updated_by: updated_by,
        updated_at: new Date(),
    };

    // cek properti lain yang terdapat nilai
    const properties = [
        `employee_code `,
        `fullname`,
        `place_birth`,
        `date_birth`,
        `gender`,
        `position`,
        `gaji_pokok`,
        `tunjangan`,
        `fixedComponents`,
        `activated`,
    ];
    for (var key in req.body) {
        if (properties.includes(key)) {
            var value = req.body[key] ? req.body[key] : null;
            if (value) {
                if (key == `activated`) {
                    data[key] = value == true || value == "true";
                } else if (key == "date_birth") {
                    data[key] = new Date(value);
                } else if (key == "fixedComponents") {
                    data["fixed_component"] = JSON.stringify(value);
                } else {
                    data[key] = value;
                }
            }
        }
    }

    const employee_code = data.employee_code
        ? data.employee_code.toUpperCase()
        : null;
    if (employee_code) {
        const available = await knex
            .from(KARYAWAN_TABLE)
            .select("_id")
            .where("employee_code", employee_code)
            .first();
        if (available) {
            const old_id = available._id ? available._id : null;
            if (old_id != _id) {
                return res.code(400).send({
                    statusCode: 400,
                    error: "Bad request",
                    message: {
                        id: "Kode karyawan sudah digunakan",
                        en: "Employee code already used",
                    },
                });
            }
        }
    }

    // menyimpan data berdasarkan properti yang terisi
    try {
        await knex
            .from(KARYAWAN_TABLE)
            .update(data)
            .where({ _id: _id })
            .orWhere({ employee_code: _id });
    } catch (error) {
        console.log(error);
    }

    return res.code(200).send({
        statusCode: 200,
        message: {
            id: "Data karyawan berhasil diperbarui",
            en: "Employee data updated successfully",
        },
    });
}

async function deleteData(req, res) {
    var _id = req.params._id ? req.params._id : null;

    try {
        await knex
            .from(KARYAWAN_TABLE)
            .where({ _id: _id })
            .orWhere({ employee_code: _id })
            .delete();
        return res.code(200).send({
            statusCode: 200,
            message: {
                id: "Data karyawan berhasil dihapus",
                en: "Employee data deleted successfully",
            },
        });
    } catch (error) {
        return res.code(403).send({
            statusCode: 403,
            message: {
                id: "Data karyawan tidak dapat dihapus, karena digunakan sebagai referensi data lain.",
                en: "The employee data cannot be deleted, because it is used as a reference for other data.",
            },
        });
    }
}

module.exports = {
    postData,
    getData,
    detailData,
    putData,
    deleteData,
};
