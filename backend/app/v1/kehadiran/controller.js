"use strict";

require("dotenv").config();
const knex = require("../../../db/sql-connection");
const {
    USER_TABLE,
    KARYAWAN_TABLE,
    KEHADIRAN_TABLE,
} = require("../../../db/tables");

async function postData(req, res) {
    // mendapatkan pengguna
    const created_by = req.headers.account_id;
    const data = {
        created_by: created_by,
    };

    // cek properti lain yang terdapat nilai
    const properties = [
        `employee_id`,
        `time_in`,
        `note_in`,
        `time_out`,
        `note_out`,
        `approved`,
    ];
    for (var key in req.body) {
        if (properties.includes(key)) {
            var value = req.body[key] ? req.body[key] : null;
            if (value) {
                if (key == `approved`) {
                    data[key] = value == true || value == "true";
                } else if (["time_in", "time_out"].includes(key)) {
                    data[key] = new Date(value);
                } else {
                    data[key] = value;
                }
            }
        }
    }

    var employee_id = data.employee_id ? data.employee_id : null;

    // cek properti yang wajib diisi
    if (!employee_id) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "ID karyawan wajib diisi",
                en: "Employee ID is required",
            },
        });
    } else {
        const check = await knex
            .from(KARYAWAN_TABLE)
            .where({ _id: employee_id })
            .first();

        if (check) {
            // menyimpan data berdasarkan properti yang terisi
            await knex.from(KEHADIRAN_TABLE).insert(data);

            return res.code(201).send({
                statusCode: 201,
                message: {
                    id: "Kehadiran berhasil ditambahkan",
                    en: "Attendance added successfully",
                },
            });
        } else {
            return res.code(404).send({
                statusCode: 404,
                error: "Bad Request",
                message: {
                    id: "ID karyawan tidak ditemukan",
                    en: "Employee ID not found",
                },
            });
        }
    }
}

async function getData(req, res) {
    var start_time = req.query.start_time ? req.query.start_time : null;
    var end_time = req.query.end_time ? req.query.end_time : null;
    var order_field = req.query.order_field
        ? req.query.order_field
        : "fullname";
    var order_by = req.query.order_by ? req.query.order_by : "ASC";

    const select = ["A.*", "B.employee_code", "B.fullname", "B.position"];
    var returnData = [];
    if (start_time && end_time) {
        start_time = new Date(start_time);
        end_time = new Date(end_time);
        returnData = await knex
            .select(select)
            .from(`${KEHADIRAN_TABLE} AS A`)
            .leftJoin(`${KARYAWAN_TABLE} AS B`, "A.employee_id", "B._id")
            .where("time_in", ">=", start_time)
            .where("time_out", "<=", end_time)
            .orderBy(order_field, order_by);
    } else {
        returnData = await knex
            .select(select)
            .from(`${KEHADIRAN_TABLE} AS A`)
            .leftJoin(`${KARYAWAN_TABLE} AS B`, "A.employee_id", "B._id")
            .orderBy(order_field, order_by);
    }

    return res.code(200).send({
        statusCode: 200,
        data: returnData,
    });
}

async function putData(req, res) {
    // mendapatkan ID berita
    var _id = req.params._id ? req.params._id : null;

    // mendapatkan pengguna
    const updated_by = req.headers.account_id;
    const data = {
        updated_by: updated_by,
        updated_at: new Date(),
    };

    // cek properti lain yang terdapat nilai
    const properties = [
        `employee_id`,
        `time_in`,
        `note_in`,
        `time_out`,
        `note_out`,
        `approved`,
    ];
    for (var key in req.body) {
        if (properties.includes(key)) {
            var value = req.body[key] ? req.body[key] : null;
            if (value) {
                if (key == `approved`) {
                    data[key] = value == true || value == "true";
                } else if (["time_in", "time_out"].includes(key)) {
                    data[key] = new Date(value);
                } else {
                    data[key] = value;
                }
            }
        }
    }

    var employee_id = data.employee_id ? data.employee_id : null;

    /// cek properti yang wajib diisi
    if (!employee_id) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "ID karyawan wajib diisi",
                en: "Employee ID is required",
            },
        });
    } else {
        const check = await knex
            .from(KARYAWAN_TABLE)
            .where({ _id: employee_id })
            .first();

        if (check) {
            // menyimpan data berdasarkan properti yang terisi
            await knex.from(KEHADIRAN_TABLE).where({ _id: _id }).update(data);

            return res.code(201).send({
                statusCode: 201,
                message: {
                    id: "Kehadiran berhasil diperbarui",
                    en: "Attendance updated successfully",
                },
            });
        } else {
            return res.code(404).send({
                statusCode: 404,
                error: "Bad Request",
                message: {
                    id: "ID karyawan tidak ditemukan",
                    en: "Employee ID not found",
                },
            });
        }
    }
}

module.exports = {
    postData,
    getData,
    putData,
};
