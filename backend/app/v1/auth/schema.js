"use strict";
const table = require("../../../db/tables");
const multer = require("fastify-multer");
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, table.IMAGE_ASSETS);
    },
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}_${file.originalname.replace(/\s/g, "")}`);
    },
});

const upload = multer({ storage: storage });

const tags = ["Authentication"];

const login = {
    schema: {
        tags,
        body: {
            required: ["username", "password"],
            type: "object",
            properties: {
                username: {
                    type: "string",
                    description: "username",
                },
                password: { type: "string", description: "kata sandi" },
            },
        },
        response: {
            200: {
                description: "Successful response",
                required: ["statusCode", "token", "login"],
                type: "object",
                properties: {
                    statusCode: {
                        type: "integer",
                        example: 200,
                    },
                    token: { type: "string" },
                    login: { type: "boolean", example: true },
                },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = false;
        done();
    },
};

const profile = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "read-profile";
        done();
    },
};

const putProfile = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        consumes: ["multipart/form-data"],
        body: {
            type: "object",
            properties: {
                fullname: {
                    type: "string",
                    description: "Nama lengkap",
                },
                username: {
                    type: "string",
                },
                password: {
                    type: "string",
                },
                file_photo: {
                    type: "file",
                },
                email: {
                    type: "string",
                },
            },
        },
        response: {
            200: {
                description: "Updated",
                type: "object",
                required: ["statusCode", "message"],
                properties: {
                    statusCode: {
                        type: "integer",
                        example: 200,
                    },
                    message: {
                        type: "string",
                        example: "Profil pengguna berhasil diperbarui",
                    },
                },
            },
        },
    },
    preHandler: upload.single("file_photo"),
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "profile";
        req.headers.url_type = "update";
        done();
    },
};

const getMenu = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "read-profile";
        done();
    },
};

const cekRole = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "read-profile";
        done();
    },
};

const logout = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        response: {
            200: {
                description: "Successful response",
                type: "object",
                required: ["statusCode", "token", "login"],
                properties: {
                    statusCode: {
                        type: "integer",
                        example: 200,
                    },
                    token: { type: "string" },
                    login: { type: "boolean", example: false },
                },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "read-profile";
        done();
    },
};

module.exports = {
    login,
    profile,
    putProfile,
    getMenu,
    cekRole,
    logout,
};
