"use strict";

require("dotenv").config();
const knex = require("../../../db/sql-connection");
var hasher = require("node-object-hash")({ alg: "sha512" });
const {
    USER_TABLE,
    USER_LOGIN_TABLE,
    USER_LEVEL_TABLE,
    MODULE_TABLE,
    RULE_TABLE,
} = require("../../../db/tables");

async function login(req, res) {
    var username = req.body ? req.body.username : null;
    var password = req.body ? req.body.password : null;
    var user_agent = req.headers["user-agent"]
        ? req.headers["user-agent"]
        : null;
    var ip_address = req.ip;
    if (!username) {
        // cek username kosong atau tidak
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Username wajib diisi",
                en: "Username is required",
            },
        });
    } else if (!password) {
        // cek password kosong atau tidak
        return res.code(400).send({
            statusCode: 400,
            error: "Bad request",
            message: {
                id: "Kata sandi wajib diisi",
                en: "Password is required",
            },
        });
    } else if (username && password) {
        // ketika username dan password tidak kosong
        var where = {
            password: hasher.hash({
                payload: password,
            }),
        };
        const result = await knex.from(USER_TABLE).where(where).first();
        if (result != null) {
            if (result.activated == 1 || result.activated == true) {
                const device = await knex
                    .from(USER_LOGIN_TABLE)
                    .where({ user_id: result._id, user_agent: user_agent })
                    .first();

                var token_data = "";
                if (device != null) {
                    token_data = device.token;
                } else {
                    let token_created = new Date();
                    token_data = hasher.hash({
                        created: token_created,
                        user_id: result._id,
                    });
                    var login_data = {
                        user_id: result._id,
                        token: token_data,
                        user_agent: user_agent,
                        ip_address: ip_address,
                        created_at: token_created,
                    };

                    await knex.from(USER_LOGIN_TABLE).insert(login_data);
                }

                // auth berhasil dan response berupa token
                return res.code(200).send({
                    statusCode: 200,
                    token: token_data,
                    login: true,
                });
            } else {
                // Response akun tidak aktif
                return res.code(406).send({
                    statusCode: 406,
                    error: "Not Acceptable",
                    message: {
                        id: "Akun tidak aktif",
                        en: "Inactive Account",
                    },
                });
            }
        } else {
            // Response username atau password tidak sesuai
            return res.code(406).send({
                statusCode: 406,
                error: "Not Acceptable",
                message: {
                    id: "Username atau password tidak sesuai",
                    en: "Username or password does not match",
                },
            });
        }
    }
}

async function profile(req, res) {
    var account_id = req.headers.account_id;
    var filter = {
        "A._id": account_id,
    };
    const select = [
        "A._id",
        "A.level_id",
        "B.alias AS level",
        "A.username",
        "A.fullname",
        "A.photo",
        "A.email",
    ];
    const data = await knex
        .select(select)
        .from(`${USER_TABLE} AS A`)
        .leftJoin(`${USER_LEVEL_TABLE} AS B`, "A.level_id", "B._id")
        .where(filter)
        .first();

    const profile = {
        _id: data._id,
        username: data.username,
        fullname: data.fullname,
        photo: data.photo,
        email: data.email,
        level: data.level,
    };

    return res.code(200).send({
        statusCode: 200,
        user: profile,
    });
}

async function putProfile(req, res) {
    // mendapatkan pengguna
    const _id = req.headers.account_id;
    const data = {
        updated_by: _id,
        updated_at: new Date(),
    };

    // mendapatkan link gambar
    if (req.file) {
        const image_link = `${process.env.BASE_URL}/${req.file.destination}${req.file.filename}`;
        data[`photo`] = image_link;
    }

    // cek properti lain yang terdapat nilai
    const properties = [
        `fullname`,
        `username`,
        `password`,
        `email`,
        `link_photo`,
        `user_reference`,
    ];
    for (var key in req.body) {
        if (properties.includes(key)) {
            if (key == `link_photo` && !req.file) {
                data[`photo`] = req.body[key];
            } else if (key == `password`) {
                data[`password`] = hasher.hash({
                    payload: req.body[key],
                });
            } else {
                data[key] = req.body[key];
            }
        }
    }

    const user_name = data.username ? data.username : null;
    if (user_name) {
        const available = await knex
            .from(USER_TABLE)
            .select("_id")
            .where("username", user_name)
            .first();
        if (available) {
            const old_id = available._id ? available._id : null;
            if (old_id != _id) {
                return res.code(400).send({
                    statusCode: 400,
                    error: "bad request",
                    message: "Username sudah digunakan",
                });
            }
        }
    }

    // jika ada photo baru, hapus photo lama
    if (req.file) {
        try {
            fs.unlinkSync(`${req.file.destination}${req.file.filename}`);
        } catch (error) {}
    }

    const where = {
        _id: _id,
    };
    // menyimpan data berdasarkan properti yang terisi
    await knex.from(USER_TABLE).update(data).where(where);

    return res.code(200).send({
        statusCode: 200,
        message: "Profil pengguna berhasil diperbarui",
    });
}

async function getMenu(req, res) {
    const account_id = req.headers.account_id;
    const where = {
        "C._id": account_id,
        "A.is_menu": 1,
        "A.activated": 1,
    };
    const result = await knex
        .select("A.*")
        .from(`${MODULE_TABLE} AS A`)
        .join(`${RULE_TABLE} AS B`, "A._id", "B.module_id")
        .join(`${USER_TABLE} AS C`, "C.level_id", "B.level_id")
        .where(where)
        .orderBy("A.sequence");

    return res.code(200).send(result);
}

async function cekRole(req, res) {
    const account_id = req.headers.account_id;
    const menu_id = req.query.menu;
    const where = {
        "C._id": account_id,
        "A.parent_id": menu_id,
        "A.activated": 1,
    };
    const result = await knex
        .select("A.slug")
        .from(`${MODULE_TABLE} AS A`)
        .join(`${RULE_TABLE} AS B`, "A._id", "B.module_id")
        .join(`${USER_TABLE} AS C`, "C.level_id", "B.level_id")
        .where(where);

    return res.code(200).send(result);
}

async function logout(req, res) {
    var token = req.headers.authorization;
    var filter = {
        token: token,
    };
    await knex.from(USER_LOGIN_TABLE).where(filter).delete();
    return res.code(200).send({
        statusCode: 200,
        token: token,
        login: false,
    });
}

module.exports = {
    login,
    profile,
    putProfile,
    getMenu,
    cekRole,
    logout,
};
