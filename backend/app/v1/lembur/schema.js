"use strict";

const tags = ["Lembur"];

const postData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        body: {
            type: "object",
            required: ["employee_id", "time_start"],
            properties: {
                employee_id: {
                    type: "string",
                    description: "ID karyawan",
                },
                time_start: {
                    type: "string",
                    format: "date-time",
                    description: "Tanggal, jam masuk",
                },
                note_in: {
                    type: "string",
                    description: "Catatan masuk",
                },
                time_end: {
                    type: "string",
                    format: "date-time",
                    description: "Tanggal, jam keluar",
                },
                note_out: {
                    type: "string",
                    description: "Catatan masuk",
                },
                approved: {
                    type: "boolean",
                },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "create-attendance";
        done();
    },
};

const getData = {
    schema: {
        tags,
        query: {
            type: "object",
            properties: {
                start_date: {
                    type: "string",
                    format: "date-time",
                    description: "Tanggal mulai",
                },
                end_date: {
                    type: "string",
                    format: "date-time",
                    description: "Tanggal selesai",
                },
                order_field: {
                    type: "string",
                    description: "Default [time_start]",
                    enum: ["time_start ", "created_at", "updated_at"],
                },
                order_by: {
                    type: "string",
                    description: "Default [DESC]",
                    enum: ["ASC", "DESC"],
                },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = false;
        done();
    },
};

const putData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        params: {
            type: "object",
            required: ["_id"],
            properties: {
                _id: { type: "string", description: "ID karyawan" },
            },
        },
        body: {
            type: "object",
            properties: {
                employee_id: {
                    type: "string",
                    description: "ID karyawan",
                },
                time_start: {
                    type: "string",
                    format: "date-time",
                    description: "Tanggal, jam masuk",
                },
                note_in: {
                    type: "string",
                    description: "Catatan masuk",
                },
                time_end: {
                    type: "string",
                    format: "date-time",
                    description: "Tanggal, jam keluar",
                },
                note_out: {
                    type: "string",
                    description: "Catatan masuk",
                },
                approved: {
                    type: "boolean",
                },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "update-attendance";
        done();
    },
};

module.exports = {
    postData,
    getData,
    putData,
};
