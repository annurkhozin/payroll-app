"use strict";
const table = require("../../../db/tables");
const multer = require("fastify-multer");
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, table.IMAGE_ASSETS);
    },
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}_${file.originalname.replace(/\s/g, "")}`);
    },
});

const upload = multer({ storage: storage });

const tags = ["User"];

const postData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        consumes: ["multipart/form-data"],
        body: {
            type: "object",
            required: ["fullname", "username", "password", "level"],
            properties: {
                fullname: {
                    type: "string",
                    description: "Nama lengkap",
                },
                username: {
                    type: "string",
                },
                password: {
                    type: "string",
                },
                file_photo: {
                    type: "file",
                },
                link_photo: {
                    type: "string",
                    format: "url",
                },
                email: {
                    type: "string",
                },
                user_reference: {
                    type: "string",
                    format: "uuid",
                },
                level: {
                    type: "string",
                    format: "uuid",
                    description: "ID level",
                },
                activated: { type: "boolean", description: "Default [true]" },
            },
        },
    },
    preHandler: upload.single("file_photo"),
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "create-users";
        done();
    },
};

const getData = {
    schema: {
        tags,
        query: {
            type: "object",
            properties: {
                activated: { type: "boolean" },
                search: { type: "string" },
                limit: { type: "integer", description: "Default [10]" },
                page: { type: "integer" },
                order_field: {
                    type: "string",
                    description: "Default [fullname]",
                    enum: ["fullname ", "created_at", "updated_at"],
                },
                order_by: {
                    type: "string",
                    description: "Default [DESC]",
                    enum: ["ASC", "DESC"],
                },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = false;
        done();
    },
};

const detailData = {
    schema: {
        tags,
        params: {
            type: "object",
            required: ["_id"],
            properties: {
                _id: { type: "string", description: "ID akun pengguna" },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = false;
        done();
    },
};

const putData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        consumes: ["multipart/form-data"],
        params: {
            type: "object",
            required: ["_id"],
            properties: {
                _id: { type: "string", format: "uuid" },
            },
        },
        body: {
            type: "object",
            properties: {
                fullname: {
                    type: "string",
                    description: "Nama lengkap",
                },
                username: {
                    type: "string",
                },
                password: {
                    type: "string",
                },
                file_photo: {
                    type: "file",
                },
                link_photo: {
                    type: "string",
                    format: "url",
                },
                email: {
                    type: "string",
                },
                user_reference: {
                    type: "string",
                    format: "uuid",
                },
                level: {
                    type: "string",
                    format: "uuid",
                    description: "ID level",
                },
                activated: { type: "boolean", description: "Default [true]" },
            },
        },
    },
    preHandler: upload.single("file_photo"),
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "update-users";
        done();
    },
};

const deleteData = {
    schema: {
        tags,
        security: [{ ApiToken: [] }],
        params: {
            type: "object",
            required: ["_id"],
            properties: {
                _id: { type: "string", format: "uuid" },
            },
        },
    },
    onRequest: function (req, res, done) {
        req.headers.protected = true;
        req.headers.url_slug = "delete-users";
        done();
    },
};

module.exports = {
    postData,
    getData,
    detailData,
    putData,
    deleteData,
};
