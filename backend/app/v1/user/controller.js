"use strict";

require("dotenv").config();
const knex = require("../../../db/sql-connection");
var hasher = require("node-object-hash")({ alg: "sha512" });
const fs = require("fs");
const {
    USER_TABLE,
    IMAGE_ASSETS,
    USER_LEVEL_TABLE,
} = require("../../../db/tables");

async function postData(req, res) {
    // mendapatkan pengguna
    const created_by = req.headers.account_id;
    const data = {
        created_by: created_by,
    };

    // mendapatkan link gambar
    if (req.file) {
        const image_link = `${process.env.BASE_URL}/${req.file.destination}${req.file.filename}`;
        data[`photo`] = image_link;
    }

    // cek properti lain yang terdapat nilai
    const properties = [
        `fullname`,
        `username`,
        `password`,
        `email`,
        `link_photo`,
        `user_reference`,
        `level`,
        `activated`,
    ];
    for (var key in req.body) {
        if (properties.includes(key)) {
            if (key == `activated`) {
                data[key] = req.body[key] == `true`;
            } else if (key == `link_photo` && !req.file) {
                data[`photo`] = req.body[key];
            } else if (key == `password`) {
                data[`password`] = hasher.hash({
                    payload: req.body[key],
                });
            } else {
                data[key] = req.body[key];
            }
        }
    }

    var fullname = data.fullname ? data.fullname : null;
    var username = data.username ? data.username : null;
    var password = data.password ? data.password : null;
    var level = data.level_id ? data.level_id : null;
    var photo = data.photo ? data.photo : null;
    if (!photo) {
        data[`photo`] = `${process.env.BASE_URL}/${IMAGE_ASSETS}avatar.png`;
    }

    const user_name = data.username ? data.username : null;
    if (user_name) {
        const available = await knex
            .from(USER_TABLE)
            .select("_id")
            .where("username", user_name)
            .first();
        if (available) {
            return res.code(400).send({
                statusCode: 400,
                error: "bad request",
                message: {
                    id: "Username sudah digunakan",
                    en: "Username already used",
                },
            });
        }
    }

    // cek properti yang wajib diisi
    if (!fullname) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Nama pengguna wajib diisi",
                en: "Fullname is required",
            },
        });
    } else if (!username) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Username wajib diisi",
                en: "Username is required",
            },
        });
    } else if (!password) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Kata sandi wajib diisi",
                en: "Password is required",
            },
        });
    } else if (!level) {
        return res.code(400).send({
            statusCode: 400,
            error: "Bad Request",
            message: {
                id: "Level pengguna wajib diisi",
                en: "User level is required",
            },
        });
    } else {
        // menyimpan data berdasarkan properti yang terisi
        await knex.from(USER_TABLE).insert(data);

        return res.code(201).send({
            statusCode: 201,
            message: {
                id: "Akun pengguna berhasil ditambahkan",
                en: "User account added successfully",
            },
        });
    }
}

async function getData(req, res) {
    var activated = req.query.activated ? req.query.activated : null;
    var search = req.query.search ? req.query.search : null;
    var limit = req.query.limit ? req.query.limit : 10;
    var page = req.query.page ? req.query.page : 1;
    var order_field = req.query.order_field
        ? req.query.order_field
        : "fullname";
    var order_by = req.query.order_by ? req.query.order_by : "DESC";

    let offset = limit * (page - 1);
    let countData = 0;
    let countFilterData = 0;
    var resData = [];

    var where = {};

    if (activated !== null) {
        where = {
            "A.activated": activated == `true`,
        };
    }
    const select = [
        "A.*",
        "B.fullname AS created",
        "C.fullname AS updated",
        "D.alias",
    ];

    // cari jumlah data keseluruhan
    countData = await knex
        .from(`${USER_TABLE} AS A`)
        .count("A._id as total")
        .where(where)
        .then((data) => {
            return data[0]["total"];
        });

    // ketika dilakukan search data
    if (search) {
        // cari jumlah data ditemukan sesuai kata kunci
        countFilterData = await knex
            .from(`${USER_TABLE} AS A`)
            .where(where)
            .andWhere("A.fullname", "LIKE", "%" + search + "%")
            .count("A._id as total")
            .then((data) => {
                return data[0]["total"];
            });

        // cari data sesuai kata kunci
        resData = await knex
            .select(select)
            .from(`${USER_TABLE} AS A`)
            .leftJoin(`${USER_TABLE} AS B`, "A.created_by", "B._id")
            .leftJoin(`${USER_TABLE} AS C`, "A.updated_by", "C._id")
            .leftJoin(`${USER_LEVEL_TABLE} AS D`, "A.level_id", "D._id")
            .where(where)
            .andWhere("A.fullname", "LIKE", "%" + search + "%")
            .offset(offset)
            .limit(limit)
            .orderBy(order_field, order_by);
    } else {
        // ketika tidak dilakukan search data
        countFilterData = countData;
        resData = await knex
            .select(select)
            .from(`${USER_TABLE} AS A`)
            .leftJoin(`${USER_TABLE} AS B`, "A.created_by", "B._id")
            .leftJoin(`${USER_TABLE} AS C`, "A.updated_by", "C._id")
            .leftJoin(`${USER_LEVEL_TABLE} AS D`, "A.level_id", "D._id")
            .where(where)
            .offset(offset)
            .limit(limit)
            .orderBy(order_field, order_by);
    }

    const returnData = [];
    resData.forEach((key) => {
        const data = {
            _id: key._id,
            fullname: key.fullname,
            username: key.username,
            password: key.password,
            photo: key.photo,
            email: key.email,
            user_reference: key.user_reference,
            level: {
                level_id: key.level_id,
                alias: key.alias,
            },
            activated: key.activated,
            created_at: key.created_at,
            updated_at: key.updated_at,
            created_by: {
                account_id: key.created_by,
                fullname: key.created,
            },
            updated_by: {
                account_id: key.updated_by,
                fullname: key.updated,
            },
        };
        returnData.push(data);
    });

    return res.code(200).send({
        statusCode: 200,
        recordsTotal: countData,
        recordsFiltered: countFilterData,
        limit: limit,
        page: page,
        data: returnData,
    });
}

async function detailData(req, res) {
    // mendapatakan ID params
    var _id = req.params._id ? req.params._id : null;

    const where = {
        "A._id": _id,
    };

    const select = [
        "A.*",
        "B.fullname AS created",
        "C.fullname AS updated",
        "D.alias",
    ];
    const data = await knex
        .select(select)
        .from(`${USER_TABLE} AS A`)
        .leftJoin(`${USER_TABLE} AS B`, "A.created_by", "B._id")
        .leftJoin(`${USER_TABLE} AS C`, "A.updated_by", "C._id")
        .leftJoin(`${USER_LEVEL_TABLE} AS D`, "A.level_id", "D._id")
        .where(where)
        .first();

    if (data) {
        const returnData = {
            _id: data._id,
            fullname: data.fullname,
            username: data.username,
            password: data.password,
            photo: data.photo,
            email: data.email,
            user_reference: data.user_reference,
            level: {
                level_id: data.level_id,
                alias: data.alias,
            },
            activated: data.activated,
            created_at: data.created_at,
            updated_at: data.updated_at,
            created_by: {
                account_id: data.created_by,
                fullname: data.created,
            },
            updated_by: {
                account_id: data.updated_by,
                fullname: data.updated,
            },
        };
        return res.code(200).send({
            statusCode: 200,
            data: returnData,
        });
    } else {
        return res.code(404).send({
            statusCode: 404,
            error: "Not found",
            message: {
                id: "ID pengguna tidak ditemukan",
                en: "User ID not found",
            },
        });
    }
}

async function putData(req, res) {
    // mendapatkan ID berita
    var _id = req.params._id ? req.params._id : null;

    // mendapatkan pengguna
    const updated_by = req.headers.account_id;
    const data = {
        updated_by: updated_by,
        updated_at: new Date(),
    };

    // mendapatkan link gambar
    if (req.file) {
        const image_link = `${process.env.BASE_URL}/${req.file.destination}${req.file.filename}`;
        data[`photo`] = image_link;
    }

    // cek properti lain yang terdapat nilai
    const properties = [
        `fullname`,
        `username`,
        `password`,
        `email`,
        `link_photo`,
        `user_reference`,
        `level`,
        `activated`,
    ];
    for (var key in req.body) {
        if (properties.includes(key)) {
            if (key == `activated`) {
                data[key] = req.body[key] == `true`;
            } else if (key == `link_photo` && !req.file) {
                data[`photo`] = req.body[key];
            } else if (key == `password`) {
                data[`password`] = hasher.hash({
                    payload: req.body[key],
                });
            } else {
                data[key] = req.body[key];
            }
        }
    }

    const user_name = data.username ? data.username : null;
    if (user_name) {
        const available = await knex
            .from(USER_TABLE)
            .select("_id")
            .where("username", user_name)
            .first();
        if (available) {
            const old_id = available._id ? available._id : null;
            if (old_id != _id) {
                return res.code(400).send({
                    statusCode: 400,
                    error: "Bad request",
                    message: {
                        id: "Username sudah digunakan",
                        en: "Username already used",
                    },
                });
            }
        }
    }

    const where = {
        _id: _id,
    };

    // jika ada photo baru, hapus photo lama
    if (data.photo) {
        const data_photo = await knex
            .select("photo")
            .from(USER_TABLE)
            .where(where)
            .first();

        if (data_photo) {
            var old_photo = data_photo.photo;
            if (old_photo) {
                const fileDir = old_photo.split("/");
                const filename = fileDir[fileDir.length - 1];
                if (
                    filename != "logo.png" &&
                    filename != "avatar.png" &&
                    old_photo != data.photo
                ) {
                    try {
                        fs.unlinkSync(`${table.IMAGE_ASSETS}${filename}`);
                    } catch (error) {}
                }
            }
        }
    }

    // menyimpan data berdasarkan properti yang terisi
    await knex.from(USER_TABLE).update(data).where(where);

    return res.code(200).send({
        statusCode: 200,
        message: {
            id: "Akun pengguna berhasil diperbarui",
            en: "User account updated successfully",
        },
    });
}

async function deleteData(req, res) {
    var _id = req.params._id ? req.params._id : null;
    const where = {
        _id: _id,
    };
    const data_photo = await knex
        .select("photo")
        .from(USER_TABLE)
        .where(where)
        .first();

    try {
        await knex.from(USER_TABLE).where(where).delete();
        if (data_photo) {
            var old_photo = data_photo.photo;
            if (old_photo) {
                const fileDir = old_photo.split("/");
                const filename = fileDir[fileDir.length - 1];
                if (filename != "logo.png" && filename != "avatar.png") {
                    try {
                        fs.unlinkSync(`${table.IMAGE_ASSETS}${filename}`);
                    } catch (error) {}
                }
            }
        }
        return res.code(200).send({
            statusCode: 200,
            message: {
                id: "Akun pengguna berhasil dihapus",
                en: "User account deleted successfully",
            },
        });
    } catch (error) {
        return res.code(403).send({
            statusCode: 403,
            message: {
                id: "Akun pengguna tidak dapat dihapus, karena digunakan sebagai referensi data lain.",
                en: "The user account cannot be deleted, because it is used as a reference for other data.",
            },
        });
    }
}

module.exports = {
    postData,
    getData,
    detailData,
    putData,
    deleteData,
};
