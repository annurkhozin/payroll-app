"use strict";

const Auth = require("./auth/controller");
const AuthSchema = require("./auth/schema");

const User = require("./user/controller");
const UserSchema = require("./user/schema");

const Employee = require("./karyawan/controller");
const EmployeeSchema = require("./karyawan/schema");

const Hadir = require("./kehadiran/controller");
const HadirSchema = require("./kehadiran/schema");

const Lembur = require("./lembur/controller");
const LemburSchema = require("./lembur/schema");

const Payroll = require("./gaji/controller");
const PayrollSchema = require("./gaji/schema");

async function routes(app) {
    // Authentication
    app.post("/auth/login", AuthSchema.login, Auth.login);
    app.get("/auth/profile", AuthSchema.profile, Auth.profile);
    app.put("/auth/profile", AuthSchema.putProfile, Auth.putProfile);
    app.get("/auth/menu", AuthSchema.getMenu, Auth.getMenu);
    app.get("/auth/role", AuthSchema.cekRole, Auth.cekRole);
    app.delete("/auth/logout", AuthSchema.logout, Auth.logout);

    // User
    app.post("/users", UserSchema.postData, User.postData);
    app.get("/users", UserSchema.getData, User.getData);
    app.put("/users/:_id", UserSchema.putData, User.putData);
    app.delete("/users/:_id", UserSchema.deleteData, User.deleteData);

    // User
    app.post("/employees", EmployeeSchema.postData, Employee.postData);
    app.get("/employees", EmployeeSchema.getData, Employee.getData);
    app.get("/employees/:_id", EmployeeSchema.detailData, Employee.detailData);
    app.put("/employees/:_id", EmployeeSchema.putData, Employee.putData);
    app.delete(
        "/employees/:_id",
        EmployeeSchema.deleteData,
        Employee.deleteData
    );

    // Kehadiran
    app.post("/attendance", HadirSchema.postData, Hadir.postData);
    app.get("/attendance", HadirSchema.getData, Hadir.getData);
    app.put("/attendance/:_id", HadirSchema.putData, Hadir.putData);

    // Lembur
    app.post("/overtime", LemburSchema.postData, Lembur.postData);
    app.get("/overtime", LemburSchema.getData, Lembur.getData);
    app.put("/overtime/:_id", LemburSchema.putData, Lembur.putData);

    // Payroll
    app.post("/payroll", PayrollSchema.postData, Payroll.postData);
    app.get("/payroll", PayrollSchema.getData, Payroll.getData);
    app.put("/payroll", PayrollSchema.putData, Payroll.putData);
    app.post(
        "/payroll/download",
        PayrollSchema.downloadSlip,
        Payroll.downloadSlip
    );
    app.get("/payroll/my", PayrollSchema.getSlipGaji, Payroll.getSlipGaji);
}

module.exports = routes;
