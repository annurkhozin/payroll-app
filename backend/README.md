## Backend Restful API Payroll App

### Setup

Use npm command to install package

```js
npm install
```

### Migrations & Seed

Command to start the migration table & seed data.

```js
npx knex migrate:latest
npx knex seed:run
```

### Run Application

Command to run serve with hot reload at [http://127.0.0.1:3031](http://127.0.0.1:3031)

```js
npm run dev
```

or use `npm run start` to build for production and launch server.

### Documentation API ( ./docs )

[http://127.0.0.1:3031/docs](http://127.0.0.1:3031/docs)
