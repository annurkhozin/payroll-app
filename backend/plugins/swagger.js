"use strict";

require("dotenv").config();

exports.options = {
    routePrefix: "/docs",
    exposeRoute: true,
    hideUntagged: true,
    swagger: {
        info: {
            title: "RESTFul API Payroll App",
            description:
                "WELCOME to the Payroll Application API documentation.",
            version: "1.0.0",
            contact: {
                name: "Nur Khozin",
                email: "annurkhozin@gmail.com",
            },
        },
        host: "127.0.0.1:" + process.env.PORT,
        schemes: ["http", "https"],
        consumes: ["application/x-www-form-urlencoded"],
        produces: ["application/json"],
        tags: [
            {
                name: "Main",
                description: "Main end-points",
            },
            {
                name: "Authentication",
                description: "Authentication module related end-points",
            },
            {
                name: "User",
                description: "User module related end-points",
            },
            {
                name: "Karyawan",
                description: "Employee module related end-points",
            },
            {
                name: "Kehadiran",
                description: "Attendance module related end-points",
            },
            {
                name: "Lembur",
                description: "Overtime module related end-points",
            },
            {
                name: "Penggajian",
                description: "Payroll module related end-points",
            },
        ],
        withCredentials: true,
        securityDefinitions: {
            ApiToken: {
                description: "Authorization header token",
                type: "apiKey",
                name: "Authorization",
                in: "header",
            },
        },
    },
};
