export default async (context, locale) => {
  return await Promise.resolve({
    Hello: "Halo",
    Welcome_back: "Selamat datang kembali",
    Home: "Beranda",
    Hi_friend: "Hai sobat,",
    Sub_hi_friend: "Silahkan lakukan otorisasi terlebih dulu",
    Password: "Kata sandi",
    Forgot_password: "Lupa kata sandi",
    Login: "Masuk",
    Send: "Kirim",
    About: "Tentang kami",
    Sub_hi_forgot: "Masukkan email Anda",
    Remember_password: "Ingat kata Sandi",
    Profile: "Profil",
    Setting: "Pengaturan",
    Logout: "Keluar",
    User: "Pengguna",
    Search: "Cari",
    Show: "Tampilkan",
    Showing: "Menampilkan",
    of: "dari",
    Add: "Tambah",
    New: "Baru",
    Back: "Kembali",
    Ok: "Oke",
    Yes: "Ya",
    No: "Tidak",
    Continue: "Lanjutkan",
    NoAvailable: "Tidak kersedia",
    reload: "muat ulang",
    AlertDemoTitle: "😥 Akses di batasi",
    AlertDemoContent: "Sayang sekali, akun DEMO tidak bisa melakukan aksi ini",
    AlertDeleteContent:
      "Akun akan di Non-aktifkan. Hal ini akan menyebabkan akun tersebut tidak dapat masuk ke Sistem",
    Confirmation: "Konfirmasi",
    LogoutMessage: "Apakah Anda yakin ingin keluar dari akun Anda?",
    Employee: "Karyawan",
    Done: "Selesai",
    Active_user: "Pengguna aktif",
    Active_level: "Level akses aktif",
    Code: "Kode",
    Fullname: "Nama lengkap",
    Position: "Jabatan",
    Loading: "Memuat",
    Actions: "Aksi",
    Update: "Ubah",
    Delete: "Hapus",
    Place_birth: "Tempat lahir",
    Date_birth: "Tanggal lahir",
    Gender: "Jenis kelamin",
    Male: "Laki-laki",
    Feemale: "Perempuan",
    Active: "Aktif",
    Inactive: "Tidak aktif",
    Access_status: "Status akses",
    Employee_status: "Status karyawan",
    Additional_components: "Komponen tambahan",
    Component_name: "Nama komponen",
    Value: "Nilai",
    Payroll: "Penggajian",
    Select_employee: "Pilih karyawan",
    Not_work: "Tidak bekerja",
    Attendance: "Kehadiran",
    Overtime: "Lembur",
    Gaji_pokok: "Gaji pokok",
    Tunjangan: "Tunjangan"
  });
};
