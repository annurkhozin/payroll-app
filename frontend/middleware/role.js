export default function ({ route, app }) {
  return app.$axios
    .$get("/auth/role?menu=" + route.query.menu)
    .then((resp) => {
      const roles = resp.map((a) => a.slug);
      app.store.commit("SET_ROLES", roles);
    })
    .catch((error) => {
      if (error.response) {
        app.$auth.logout().then(() => {
          app.$router.push("login");
        });
      }
    });
}
